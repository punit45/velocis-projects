package org.egov.bookings.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
@Entity(name = "OpenSpaceBuildingMaterialBookingsModle")
@Table(name = "TP_OPEN_SPACE_BUILDING_MATERIAL_BOOKINGS")
public class OpenSpaceBuildingMaterialBookingsModel {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "osbm_booking_id")
	private Long osbmBookingId;
	
	@Column(name = "OSBM_HOUSE_SITE_NO")
	private String osbmHouseSiteNo;
	
	@Column(name = "OSBM_ADDRESS")
	private String osbmAddress;
	
	@Column(name = "OSBM_SECTOR")
	private String osbmSector;
	
	@Column(name = "OSBM_VILL_CITY")
	private String osbmVillCity;

	@Column(name = "OSBM_AREA_REQUIRED")
	private String osbmAreaRequired;
	
	@Column(name = "OSBM_DURATION")
	private String osbmDuration;
	
	@Column(name = "OSBM_CATEGORY")
	private String osbmCategory;
	
	
	@Column(name = "OSBM_EMAIL")
	private String osbmEmail;
	
	@Column(name = "OSBM_CONTACT_NO")
	private String osbmContactNo;
	
	@Column(name = "OSBM_DOCUMENT_UPLOADED_URL")
	private String osbmDocumentUploadedUrl;
	
	@Column(name = "OSBM_DATE_CREATED")
	private Date osbmDateCreated;
	
	@Column(name = "OSBM_CREATED_BY")
	private Long osbmCreatedBy;
	
	@Column(name = "OSBM_WF_STATUS")
	private String osbmWfStatus;
	
	@Column(name = "OSBM_AMOUNT")
	private String osbmAmount;
	
	@Column(name = "OSBM_PAYMENT_STATUS")
	private String osbmPaymentStatus;
	
	@Column(name = "OSBM_PAYMENT_DATE")
	private Date osbmPaymentDate;
	
	
	
}
